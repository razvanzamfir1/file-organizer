# File Organizer

# Ce trebuie facut:

# Input Data:
#     - Path to folder
# Constants:
#     - DIR_NAMES
#     - FILE EXTENSION
# OUPUT:
#     - Move file in the specific folder


import os
import shutil
# creem meniul de lucru
# Se creaza meniul pentru a acesa cale catre folderele pe care le vom organiza 1.
def menu():
    path = input('Introduceti calea sau apasati Q pentru a iesi din program: ')
    while not path_validation(path): # False - la functia de validare cat timp nu introduci calea corecta
        if path == 'Q':
            exit()
        path = input('Introduceti calea sau apasati Q pentru a iesi din program: ')
    print('The path is valid') # True la functia de validare.
    return path

# aici validam daca calea este cea corecta cu bool 2.
def path_validation(path: str) -> bool:  # primeste ca parametru un string si returneaza un bool-ean.
    return os.path.isdir(path) # verifica daca este un director sau nu si returneaza true sau false

def list_all_files(path: str) -> list: # se genereaza lista cu toate fisierele din downloads 5.
    files = [file for file in os.listdir(path) if os.path.isfile(path + '\\' + file)]
    # os.listdir - genereaza o lista cu tot continutul folderului downloads, daca este un fisier inpune in lista
    return files

# se extrage extensia file-urilor
def extract_file_extension(file: str) -> str: #  lista primeste  elementul din lista files.se extrage extensia file-urilor 6.
    indexes = [i for i,ch in enumerate(file) if ch =='.'] # enumerate - se itereaza atat prin indeci cat si prin caractere
    if indexes:  # daca lista contine elemente
        file_extension = file[indexes[-1]::]  # se va lua in considerare ultimul elem ('.') si de acolo pana la sfarsit
        return file_extension
    else:
        return 'No extension!'
#se creaza dictionarul cu calea + elem din dir_keys(K) : elem din file_ext_type(corespondente cf index) (v) 3.
def map_extension_to_folder(path: str) -> dict:
    extension_mapping = {path+ '\\' +dir: FILE_EXT_TYPE[i] for i,dir in enumerate(DIR_TYPES)} # k:v(k=calea + dir : v=file_extension(care se mapeaza)
    return extension_mapping

def create_dirs(path: str): # se creaza directoarele in folderul din calea aleasa 4.
    for dir in DIR_TYPES:
        if not os.path.isdir(path + '\\' + dir): # daca nu se afla creat directorul in folderul dat de cale
            os.mkdir(path + '\\' + dir) # creaza un director



if __name__ == '__main__':

    DIR_TYPES = ['Pictures', 'Videos', 'PDF_files',
                 'Music', 'TXT_files', 'Python_files',
                 'Worl_files', 'Excel_files', 'Exe_files',
                 'Archives_files', 'CDR_files']

    FILE_EXT_TYPE = [['.jpg', '.jpeg', '.png', '.JPG'], ['.mp4', '.mov', '.MOV', '.avi'], ['.pdf', '.PDF'],
                     '.mp3', '.txt', '.py',
                     ['.doc', '.docx'], ['.csv', '.xlsx', '.xls'], '.exe',
                     ['.7z', '.zip'], '.cdr']

    path = menu()
    mapping = map_extension_to_folder(path)
    # print(mapping)
    create_dirs(path)
    files = list_all_files(path)
    # print(files)
    for file in files:
        file_extension = extract_file_extension(file)
        # print(file_extension)
        for k,v in mapping.items():
            if file_extension in v:  # daca fisierul (extensia se afla in v(din maapare(dictionar)
                try:
                    shutil.move(path + '\\' + file, k) #shutil este pentru mutarea fisierul nostru in folderul (dir) cheie creat
                except:
                    print(file + ' nu poate fi mutat!')


